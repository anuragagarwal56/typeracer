﻿namespace TypeRacer
{
    partial class TypeRacer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Clock = new System.Windows.Forms.Timer(this.components);
            this.timer = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.IP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Player = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Score = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.WPM = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.UserName = new System.Windows.Forms.Label();
            this.NewGameButton = new System.Windows.Forms.Button();
            this.CountDownTimer = new System.Windows.Forms.Timer(this.components);
            this.wordPerMinute = new System.Windows.Forms.Label();
            this.changeUserName = new System.Windows.Forms.Button();
            this.gameMode = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showStatsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeServerIPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeUserNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stringConstraintsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numbersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anyCaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.specialCharactersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showChatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Enabled = false;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.richTextBox1.Location = new System.Drawing.Point(19, 121);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.ShortcutsEnabled = false;
            this.richTextBox1.Size = new System.Drawing.Size(644, 163);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.textBox1.Location = new System.Drawing.Point(16, 306);
            this.textBox1.Name = "textBox1";
            this.textBox1.ShortcutsEnabled = false;
            this.textBox1.Size = new System.Drawing.Size(644, 30);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(16, 58);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(644, 33);
            this.progressBar1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "0%";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(627, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "100%";
            // 
            // Clock
            // 
            this.Clock.Enabled = true;
            this.Clock.Interval = 1000;
            this.Clock.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer
            // 
            this.timer.AutoSize = true;
            this.timer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.timer.Location = new System.Drawing.Point(610, 31);
            this.timer.Name = "timer";
            this.timer.Size = new System.Drawing.Size(49, 20);
            this.timer.TabIndex = 5;
            this.timer.Text = "00:00";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.IP,
            this.Player,
            this.Score,
            this.WPM});
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(681, 58);
            this.listView1.Name = "listView1";
            this.listView1.ShowItemToolTips = true;
            this.listView1.Size = new System.Drawing.Size(246, 278);
            this.listView1.TabIndex = 9;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // IP
            // 
            this.IP.Text = "IP";
            this.IP.Width = 0;
            // 
            // Player
            // 
            this.Player.Text = "Player";
            this.Player.Width = 122;
            // 
            // Score
            // 
            this.Score.Text = "Score";
            // 
            // WPM
            // 
            this.WPM.Text = "WPM";
            // 
            // UserName
            // 
            this.UserName.AutoSize = true;
            this.UserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.UserName.Location = new System.Drawing.Point(16, 31);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(53, 20);
            this.UserName.TabIndex = 11;
            this.UserName.Text = "Guest";
            // 
            // NewGameButton
            // 
            this.NewGameButton.Location = new System.Drawing.Point(772, 29);
            this.NewGameButton.Name = "NewGameButton";
            this.NewGameButton.Size = new System.Drawing.Size(75, 23);
            this.NewGameButton.TabIndex = 10;
            this.NewGameButton.Text = "New Game";
            this.NewGameButton.UseVisualStyleBackColor = true;
            this.NewGameButton.EnabledChanged += new System.EventHandler(this.NewGameButton_EnabledChanged);
            this.NewGameButton.Click += new System.EventHandler(this.NewGameButton_Click);
            // 
            // CountDownTimer
            // 
            this.CountDownTimer.Enabled = true;
            this.CountDownTimer.Interval = 1000;
            this.CountDownTimer.Tick += new System.EventHandler(this.CountDownTimer_Tick);
            // 
            // wordPerMinute
            // 
            this.wordPerMinute.AutoSize = true;
            this.wordPerMinute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.wordPerMinute.Location = new System.Drawing.Point(310, 31);
            this.wordPerMinute.Name = "wordPerMinute";
            this.wordPerMinute.Size = new System.Drawing.Size(59, 20);
            this.wordPerMinute.TabIndex = 13;
            this.wordPerMinute.Text = "0 Wpm";
            // 
            // changeUserName
            // 
            this.changeUserName.Location = new System.Drawing.Point(681, 29);
            this.changeUserName.Name = "changeUserName";
            this.changeUserName.Size = new System.Drawing.Size(85, 23);
            this.changeUserName.TabIndex = 14;
            this.changeUserName.Text = "Change Name";
            this.changeUserName.UseVisualStyleBackColor = true;
            this.changeUserName.EnabledChanged += new System.EventHandler(this.changeUserName_EnabledChanged);
            this.changeUserName.Click += new System.EventHandler(this.changeUserName_Click);
            // 
            // gameMode
            // 
            this.gameMode.AutoSize = true;
            this.gameMode.Checked = global::TypeRacer.Properties.Settings.Default.Multiplayer;
            this.gameMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.gameMode.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::TypeRacer.Properties.Settings.Default, "Multiplayer", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.gameMode.Location = new System.Drawing.Point(853, 33);
            this.gameMode.Name = "gameMode";
            this.gameMode.Size = new System.Drawing.Size(76, 17);
            this.gameMode.TabIndex = 16;
            this.gameMode.Text = "Multiplayer";
            this.gameMode.UseVisualStyleBackColor = true;
            this.gameMode.CheckedChanged += new System.EventHandler(this.gameMode_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameToolStripMenuItem,
            this.preferenceToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(945, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem,
            this.restartToolStripMenuItem,
            this.eToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.gameToolStripMenuItem.Text = "Game";
            // 
            // newGameToolStripMenuItem
            // 
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.newGameToolStripMenuItem.Text = "New Game";
            this.newGameToolStripMenuItem.Click += new System.EventHandler(this.NewGameButton_Click);
            // 
            // restartToolStripMenuItem
            // 
            this.restartToolStripMenuItem.Name = "restartToolStripMenuItem";
            this.restartToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.restartToolStripMenuItem.Text = "Restart";
            // 
            // eToolStripMenuItem
            // 
            this.eToolStripMenuItem.Name = "eToolStripMenuItem";
            this.eToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.eToolStripMenuItem.Size = new System.Drawing.Size(151, 22);
            this.eToolStripMenuItem.Text = "Exit";
            this.eToolStripMenuItem.Click += new System.EventHandler(this.eToolStripMenuItem_Click);
            // 
            // preferenceToolStripMenuItem
            // 
            this.preferenceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showStatsToolStripMenuItem,
            this.changeServerIPToolStripMenuItem,
            this.changeUserNameToolStripMenuItem,
            this.stringConstraintsToolStripMenuItem,
            this.showChatToolStripMenuItem});
            this.preferenceToolStripMenuItem.Name = "preferenceToolStripMenuItem";
            this.preferenceToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.preferenceToolStripMenuItem.Text = "Preference";
            // 
            // showStatsToolStripMenuItem
            // 
            this.showStatsToolStripMenuItem.Name = "showStatsToolStripMenuItem";
            this.showStatsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F10;
            this.showStatsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.showStatsToolStripMenuItem.Text = "Show Stats";
            this.showStatsToolStripMenuItem.Click += new System.EventHandler(this.showStatsToolStripMenuItem_Click);
            // 
            // changeServerIPToolStripMenuItem
            // 
            this.changeServerIPToolStripMenuItem.Name = "changeServerIPToolStripMenuItem";
            this.changeServerIPToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F9;
            this.changeServerIPToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.changeServerIPToolStripMenuItem.Text = "Change Server IP";
            this.changeServerIPToolStripMenuItem.Click += new System.EventHandler(this.chaneServerIPToolStripMenuItem_Click);
            // 
            // changeUserNameToolStripMenuItem
            // 
            this.changeUserNameToolStripMenuItem.Name = "changeUserNameToolStripMenuItem";
            this.changeUserNameToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F8;
            this.changeUserNameToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.changeUserNameToolStripMenuItem.Text = "Change User Name";
            this.changeUserNameToolStripMenuItem.Click += new System.EventHandler(this.changeUserName_Click);
            // 
            // stringConstraintsToolStripMenuItem
            // 
            this.stringConstraintsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.numbersToolStripMenuItem,
            this.anyCaseToolStripMenuItem,
            this.specialCharactersToolStripMenuItem});
            this.stringConstraintsToolStripMenuItem.Name = "stringConstraintsToolStripMenuItem";
            this.stringConstraintsToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.stringConstraintsToolStripMenuItem.Text = "String Constraints";
            // 
            // numbersToolStripMenuItem
            // 
            this.numbersToolStripMenuItem.Checked = true;
            this.numbersToolStripMenuItem.CheckOnClick = true;
            this.numbersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.numbersToolStripMenuItem.Name = "numbersToolStripMenuItem";
            this.numbersToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.numbersToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.numbersToolStripMenuItem.Text = "Numbers";
            this.numbersToolStripMenuItem.CheckedChanged += new System.EventHandler(this.numbersToolStripMenuItem_CheckedChanged);
            // 
            // anyCaseToolStripMenuItem
            // 
            this.anyCaseToolStripMenuItem.Checked = true;
            this.anyCaseToolStripMenuItem.CheckOnClick = true;
            this.anyCaseToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.anyCaseToolStripMenuItem.Name = "anyCaseToolStripMenuItem";
            this.anyCaseToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F6;
            this.anyCaseToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.anyCaseToolStripMenuItem.Text = "Any Case";
            this.anyCaseToolStripMenuItem.CheckedChanged += new System.EventHandler(this.anyCaseToolStripMenuItem_CheckedChanged);
            // 
            // specialCharactersToolStripMenuItem
            // 
            this.specialCharactersToolStripMenuItem.Checked = true;
            this.specialCharactersToolStripMenuItem.CheckOnClick = true;
            this.specialCharactersToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.specialCharactersToolStripMenuItem.Name = "specialCharactersToolStripMenuItem";
            this.specialCharactersToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.specialCharactersToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.specialCharactersToolStripMenuItem.Text = "Special Characters";
            this.specialCharactersToolStripMenuItem.CheckedChanged += new System.EventHandler(this.specialCharactersToolStripMenuItem_CheckedChanged);
            // 
            // showChatToolStripMenuItem
            // 
            this.showChatToolStripMenuItem.Enabled = false;
            this.showChatToolStripMenuItem.Name = "showChatToolStripMenuItem";
            this.showChatToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.showChatToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.showChatToolStripMenuItem.Text = "Show Chat";
            this.showChatToolStripMenuItem.Click += new System.EventHandler(this.showChatToolStripMenuItem_Click);
            // 
            // TypeRacer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(945, 356);
            this.Controls.Add(this.gameMode);
            this.Controls.Add(this.changeUserName);
            this.Controls.Add(this.wordPerMinute);
            this.Controls.Add(this.UserName);
            this.Controls.Add(this.NewGameButton);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.timer);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(951, 385);
            this.MinimumSize = new System.Drawing.Size(951, 385);
            this.Name = "TypeRacer";
            this.Text = "TypeRacer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TypeRacer_FormClosing);
            this.Load += new System.EventHandler(this.TypeRacer_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer Clock;
        private System.Windows.Forms.Label timer;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader IP;
        private System.Windows.Forms.ColumnHeader Player;
        private System.Windows.Forms.ColumnHeader Score;
        private System.Windows.Forms.Label UserName;
        private System.Windows.Forms.Button NewGameButton;
        private System.Windows.Forms.Timer CountDownTimer;
        private System.Windows.Forms.Label wordPerMinute;
        private System.Windows.Forms.ColumnHeader WPM;
        private System.Windows.Forms.Button changeUserName;
        private System.Windows.Forms.CheckBox gameMode;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restartToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showStatsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeServerIPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeUserNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stringConstraintsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numbersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anyCaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem specialCharactersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showChatToolStripMenuItem;
    }
}

