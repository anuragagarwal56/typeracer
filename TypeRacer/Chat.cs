﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TypeRacer
{
    public partial class Chat : Form
    {
        TypeRacer parentForm;

        public Chat(TypeRacer parentForm)
        {
            InitializeComponent();
            this.parentForm = parentForm;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string message = "@" + Properties.Settings.Default.UserName + ":" + " " + textBox2.Text;
            appendChat(message);
            parentForm.sendChatMessage(message);
            textBox2.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text.Trim().Length == 0)
                button1.Enabled = false;
            else
                button1.Enabled = true;
        }

        public void appendChat(string message)
        {
            textBox1.Text += message;
            textBox1.AppendText(Environment.NewLine);
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button1.PerformClick();
        }
    }
}
