﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TypeRacer
{
    public partial class Stats : Form
    {
        public Stats()
        {
            InitializeComponent();
        }

        private void Stats_Load(object sender, EventArgs e)
        {
            userName.Text = ": " + Properties.Settings.Default.UserName;
            encountered.Text = ": " + Properties.Settings.Default.StringsEncounted.ToString();
            solved.Text = ": " + Properties.Settings.Default.StringsSovled.ToString();
            wpm.Text = ": " + Properties.Settings.Default.WPM.ToString() + " Wpm";
        }
    }
}
