﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using Microsoft.VisualBasic;
using System.IO;
using System.Text.RegularExpressions;

namespace TypeRacer
{
    enum Command
    {
        BeginGame,
        LeaveGame,
        SendProgress,
        SendWPM,
        ChangeUserName,
        SendMessage,
        Null
    }

    public partial class TypeRacer : Form
    {
        private String currentText;
        private int currentIndex;
        private String[] words;
        private int currentWordIndex;
        private int seconds, minutes;

        private Socket clientSocket;
        private static String serverIP = Properties.Settings.Default.ServerIP;
        private const int udpPort = 1000;
        private int countDown;
        private List<EndPoint> endPoints;
        private bool countDownStart;
        private Chat chatWindow;

        byte[] byteData = new byte[1024];

        public TypeRacer()
        {
            InitializeComponent();
            Clock.Stop();
            changeUserName.Enabled = true;
            CountDownTimer.Stop();
            endPoints = new List<EndPoint>();
            countDownStart = false;
            UserName.Text = Properties.Settings.Default.UserName.Trim().Length != 0 ? Properties.Settings.Default.UserName : "Guest";
            stringConstraintsToolStripMenuItem.Enabled = false;
            numbersToolStripMenuItem.Checked = Properties.Settings.Default.NumberOption;
            specialCharactersToolStripMenuItem.Checked = Properties.Settings.Default.SpecialCharacterOption;
            anyCaseToolStripMenuItem.Checked = Properties.Settings.Default.AnyCaseOption;
        }

        private String getQuote()
        {
            String resposeValue = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://en.wikipedia.com/w/api.php?action=query&prop=extracts&format=xml&exsentences=3&exlimit=1&explaintext=&exsectionformat=plain&generator=random&grnnamespace=0&grnlimit=1");
            request.Method = "POST";
            request.ContentType = "application/xml";
            request.ContentLength = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            if (responseStream != null)
                                using (StreamReader reader = new StreamReader(responseStream))
                                {
                                    resposeValue = Regex.Replace(System.Net.WebUtility.HtmlDecode(Regex.Replace(reader.ReadToEnd().Split(new string[] { "<extract xml:space=\"preserve\">" }, StringSplitOptions.None)[1].Split(new string[] { "</extract>" }, StringSplitOptions.None)[0], @"[^\u0000-\u007F]", string.Empty).Replace("\n", string.Empty).Replace("*", "").Replace("|", "").Replace("/", "").Replace("\\", "")), @"\s\s+", " ").Trim();
                                }
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Check your internet connection", "Client", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (!specialCharactersToolStripMenuItem.Checked)
                resposeValue = new Regex("[^a-zA-Z0-9]").Replace(resposeValue, "");
            if(!numbersToolStripMenuItem.Checked)
                resposeValue = new Regex("[0-9]").Replace(resposeValue, "");
            if (!anyCaseToolStripMenuItem.Checked)
                resposeValue = resposeValue.ToLower();
            return resposeValue.Substring(0, Math.Min(400, resposeValue.Length));
        }

        public void changeText(String text)
        {
            richTextBox1.Text = currentText;
            currentIndex = 0;
            words = currentText.Split(' ');
            currentWordIndex = 0;
            richTextBox1.SelectAll();
            richTextBox1.SelectionColor = Color.Black;
            richTextBox1.ZoomFactor = 1;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == words[currentWordIndex] + ((currentWordIndex==words.Length-1)?"":" "))
            {
                richTextBox1.Select(currentIndex, words[currentWordIndex].Length);
                richTextBox1.SelectionColor = Color.Green;
                textBox1.Text = "";
                currentIndex += words[currentWordIndex].Length + 1;
                currentWordIndex++;
                progressBar1.Value = (int)(((currentWordIndex * 1.0 / words.Length) * 100));
                
                if(gameMode.Checked)
                    send(new Data(Command.SendProgress, progressBar1.Value.ToString()));
                
                if (currentWordIndex == words.Length)
                {
                    Properties.Settings.Default.WPM = Convert.ToInt32(new Regex( @"[^\d]+").Replace(wordPerMinute.Text, ""));
                    Properties.Settings.Default.Save();
                    textBox1.Enabled = false;
                    minutes = 0;
                    seconds = 0;
                    NewGameButton.Enabled = true;
                    Clock.Stop();
                    changeUserName.Enabled = true;
                    Properties.Settings.Default.StringsSovled++;
                    Properties.Settings.Default.Save();
                }
            }
        }

        private void OnSend(IAsyncResult ar)
        {
            try
            {
                clientSocket.EndSend(ar);
            }
            catch (ObjectDisposedException)
            { }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            try
            {
                EndPoint epReceiver = (EndPoint)new IPEndPoint(IPAddress.Any, 0);
                clientSocket.EndReceiveFrom(ar, ref epReceiver);
                Data msgReceived = new Data(byteData);
                listView1.SuspendLayout();
                switch (msgReceived.CmdCommand)
                {
                    case Command.BeginGame:
                        String[] splitStar = msgReceived.UserMessage.Split('*');
                        if (listView1.Items.Count == 0)
                        {
                            richTextBox1.Text = "\nWaiting for Other Players...";
                            richTextBox1.SelectAll();
                            richTextBox1.SelectionAlignment = HorizontalAlignment.Center;
                            richTextBox1.ZoomFactor = 1.0f;
                            richTextBox1.ZoomFactor = 2.0f;
                            richTextBox1.DeselectAll();
                        }
                        for (int i = 0; i < splitStar.Length - 1; i++)
                        {
                            listView1.Items.Add(new ListViewItem(new String[] { splitStar[i].Split('|')[0], splitStar[i].Split('|')[1], "0%", "" }));
                            endPoints.Add((EndPoint)new IPEndPoint(IPAddress.Parse(splitStar[i].Split('|')[0].Split(':')[0]), Convert.ToInt32(splitStar[i].Split('|')[0].Split(':')[1])));
                            if (listView1.Items.Count == 1)
                            {
                                countDownStart = true;
                                if (this.InvokeRequired)
                                    BeginInvoke((MethodInvoker)delegate()
                                    {
                                        CountDownTimer.Start();
                                    });
                                else
                                    CountDownTimer.Start();
                            }
                        }
                        if (splitStar[splitStar.Length - 1].Contains('/'))
                        {
                            countDown = Convert.ToInt32(splitStar[splitStar.Length - 1].Split('/')[0]);
                            currentText = splitStar[splitStar.Length - 1].Split('/')[1];
                        }
                        break;

                    case Command.LeaveGame:
                        for (int i = 0; i < endPoints.Count; i++)
                            if (endPoints[i].ToString() == epReceiver.ToString())
                            {
                                endPoints.RemoveAt(i);
                                break;
                            }
                        if (countDownStart)
                        {
                            listView1.Items[listView1.Items.IndexOf(listView1.FindItemWithText(epReceiver.ToString()))].Remove();
                            if (listView1.Items.Count == 0)
                            {
                                richTextBox1.Text = "";
                                NewGameButton.Enabled = true;
                                send(new Data(Command.LeaveGame, null));
                                CountDownTimer.Stop();
                                countDownStart = false;
                            }
                        }
                        else
                            listView1.Items[listView1.Items.IndexOf(listView1.FindItemWithText(epReceiver.ToString()))].BackColor = Color.LightGray;
                        break;

                    case Command.SendProgress:
                        listView1.Items[listView1.Items.IndexOf(listView1.FindItemWithText(epReceiver.ToString()))].SubItems[2].Text = msgReceived.UserMessage + "%";
                        break;

                    case Command.SendWPM:
                        listView1.Items[listView1.Items.IndexOf(listView1.FindItemWithText(epReceiver.ToString()))].SubItems[3].Text = msgReceived.UserMessage;
                        break;

                    case Command.ChangeUserName:
                        listView1.Items[listView1.Items.IndexOf(listView1.FindItemWithText(epReceiver.ToString()))].SubItems[1].Text = msgReceived.UserMessage;
                        break;

                    case Command.SendMessage:
                        if (chatWindow != null)
                            chatWindow.appendChat(msgReceived.UserMessage);
                        break;
                }
                listView1.ResumeLayout();
                byteData = new byte[1024];
                clientSocket.BeginReceiveFrom(byteData, 0, byteData.Length, SocketFlags.None, ref epReceiver, new AsyncCallback(OnReceive), epReceiver);
            }
            catch (ObjectDisposedException)
            { }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            seconds++;
            wordPerMinute.Text = (60 * currentWordIndex / (minutes * 60 + seconds)).ToString() + " Wpm";            
            if(gameMode.Checked)
                send(new Data(Command.SendWPM, wordPerMinute.Text));
            if(seconds==60){
                minutes++;
                seconds=0;
            }
            timer.Text = (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
        }

        private void TypeRacer_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
        }

        private void TypeRacer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to leave the game?", "Client",
                MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                if(gameMode.Checked)
                    send(new Data(Command.LeaveGame, null));
                if(clientSocket!=null)
                    clientSocket.Close();
            }
            catch (ObjectDisposedException)
            { }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        public void sendChatMessage(string message)
        {
            send(new Data(Command.SendMessage, message));
        }

        private void send(Data dataToSend)
        {
            byte[] message = dataToSend.ToByte();
            
            foreach (EndPoint ep in endPoints)
                try
                {
                    clientSocket.BeginSendTo(message, 0, message.Length, SocketFlags.None, ep, new AsyncCallback(OnSend), null);
                }
                catch (Exception ex)
                {
                    Error(ex);
                }
            if (countDownStart && countDown > 5)
                try
                {
                    clientSocket.BeginSendTo(message, 0, message.Length, SocketFlags.None, (EndPoint)new IPEndPoint(IPAddress.Parse(serverIP), udpPort), new AsyncCallback(OnSend), null);
                }
                catch (Exception ex)
                {
                    Error(ex);
                }
        }

        private void Error(Exception ex)
        {
            MessageBox.Show(ex.Message, "Client", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void NewGameButton_Click(object sender, EventArgs e)
        {
            if (chatWindow != null)
                chatWindow.Close();
            send(new Data(Command.LeaveGame, null));
            wordPerMinute.Text = "0 Wpm";
            richTextBox1.SelectAll();
            richTextBox1.SelectionColor = Color.Black;
            progressBar1.Value = 0;
            listView1.Items.Clear();
            endPoints = new List<EndPoint>();
            timer.Text = "00:00";
            NewGameButton.Enabled = false;
            if (gameMode.Checked)
            {
                try
                {
                    clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    EndPoint epServer = (EndPoint)new IPEndPoint(IPAddress.Parse(serverIP), udpPort);
                    byteData = new Data(Command.BeginGame, UserName.Text+":"+Properties.Settings.Default.WPM).ToByte();
                    Data message = new Data(byteData);
                    clientSocket.BeginSendTo(byteData, 0, byteData.Length, SocketFlags.None, epServer, new AsyncCallback(OnSend), null);
                    byteData = new byte[1024];
                    clientSocket.BeginReceiveFrom(byteData, 0, byteData.Length, SocketFlags.None, ref epServer, new AsyncCallback(OnReceive), null);
                }
                catch (Exception ex)
                {
                    Error(ex);
                }
            }
            else
            {
                changeUserName.Enabled = false;
                currentText = getQuote();
                changeText(currentText);
                textBox1.Enabled = true;
                Clock.Start();                
                seconds = 0;
                Properties.Settings.Default.StringsEncounted++;
                Properties.Settings.Default.Save();
            }
        }

        private void CountDownTimer_Tick(object sender, EventArgs e)
        {
            countDown--;
            richTextBox1.Text = "\n"+countDown.ToString();
            richTextBox1.SelectAll();
            richTextBox1.SelectionAlignment = HorizontalAlignment.Center;
            richTextBox1.DeselectAll();
            if (countDown == 0)
            {
                Properties.Settings.Default.StringsEncounted++;
                Properties.Settings.Default.Save();
                changeText(currentText);
                chatWindow = new Chat(this);
                showChatToolStripMenuItem.Enabled = true;
                chatWindow.Show(this);
                CountDownTimer.Stop();
                countDownStart = false;
                textBox1.Enabled = true;
                Clock.Start();
                changeUserName.Enabled = false;
                seconds = 0;
            }
        }

        private void changeUserName_Click(object sender, EventArgs e)
        {
            string newUserName = Microsoft.VisualBasic.Interaction.InputBox("User Name", "Change User Name", UserName.Text);
            newUserName = new Regex("[^a-zA-Z0-9]").Replace(newUserName, "");
            if (newUserName.Trim().Length != 0)
            {
                UserName.Text = newUserName.Trim();
                Properties.Settings.Default.UserName = UserName.Text;
                Properties.Settings.Default.Save();
                if(progressBar1.Value!=100 && gameMode.Checked)
                    send(new Data(Command.ChangeUserName, newUserName));
            }
        }

        private void NewGameButton_EnabledChanged(object sender, EventArgs e)
        {
            changeServerIPToolStripMenuItem.Enabled = NewGameButton.Enabled;
            newGameToolStripMenuItem.Enabled = NewGameButton.Enabled;
            if (NewGameButton.Enabled == true)
                showChatToolStripMenuItem.Enabled = true;
        }

        private void chaneServerIPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string connectionIP = Microsoft.VisualBasic.Interaction.InputBox("Server IP", "Change Server IP", serverIP);
            try
            {
                IPAddress.Parse(connectionIP);
                serverIP = connectionIP;
                Properties.Settings.Default.ServerIP = connectionIP;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        private void showStatsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Stats().ShowDialog();
        }

        private void eToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void changeUserName_EnabledChanged(object sender, EventArgs e)
        {
            changeServerIPToolStripMenuItem.Enabled = changeUserName.Enabled;
        }

        private void gameMode_CheckedChanged(object sender, EventArgs e)
        {
            stringConstraintsToolStripMenuItem.Enabled = !gameMode.Checked;
            Properties.Settings.Default.Save();
        }

        private void numbersToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.NumberOption = !Properties.Settings.Default.NumberOption;
            Properties.Settings.Default.Save();
        }

        private void anyCaseToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.AnyCaseOption = !Properties.Settings.Default.AnyCaseOption;
            Properties.Settings.Default.Save();
        }

        private void specialCharactersToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.SpecialCharacterOption = !Properties.Settings.Default.SpecialCharacterOption;
            Properties.Settings.Default.Save();
        }

        private void showChatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (chatWindow != null)
                chatWindow = new Chat(this);
            chatWindow.Show();
        }
    }

    class Data
    {
        private String userMessage;
        private Command cmdCommand;

        public String UserMessage
        {
            get
            {
                return this.userMessage;
            }
            set
            {
                this.userMessage = value;
            }
        }

        public Command CmdCommand
        {
            get
            {
                return this.cmdCommand;
            }
            set
            {
                this.cmdCommand = value;
            }
        }

        public Data()
        {
            this.cmdCommand = Command.Null;
            this.userMessage = null;
        }

        public Data(Command cmdCommand, String userMessage)
        {
            this.CmdCommand = cmdCommand;
            this.UserMessage = userMessage;
        }

        public Data(byte[] data)
        {
            this.cmdCommand = (Command)BitConverter.ToInt32(data, 0);
            int msgLen = BitConverter.ToInt32(data, 4);
            UserMessage = msgLen > 0 ? Encoding.UTF8.GetString(data, 8, msgLen) : null;
        }

        public byte[] ToByte()
        {
            List<byte> result = new List<byte>();
            result.AddRange(BitConverter.GetBytes((int)cmdCommand));
            result.AddRange(BitConverter.GetBytes(UserMessage != null ? UserMessage.Length : 0));
            if (UserMessage != null) result.AddRange(Encoding.UTF8.GetBytes(UserMessage));
            return result.ToArray();
        }
    }
}
