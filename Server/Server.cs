using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;

namespace Server
{
    enum Command
    {
        BeginGame,
        LeaveGame,
        Null
    }

    struct ClientInfo
    {
        public EndPoint endpoint;
        public String userName;

        public ClientInfo(EndPoint ep, String userName)
        {
            this.endpoint = ep;
            this.userName = userName;
        }
        public override string ToString()
        {
            return endpoint.ToString() + '|' + userName + '*';
        }
    }

    public partial class Server : Form
    {
        private Socket serverSocket;
        private const int udpPort = 1000;
        byte[] byteData = new byte[1024];
        private Game[] games;

        public Server()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                CheckForIllegalCrossThreadCalls = false;
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                serverSocket.Bind(new IPEndPoint(IPAddress.Any, udpPort));
                EndPoint epSender = (EndPoint)new IPEndPoint(IPAddress.Any, 0);
                games = new Game[10];
                for (int i = 0; i < games.Length; i++)
                    games[i] = new Game(serverSocket);
                serverSocket.BeginReceiveFrom(byteData, 0, byteData.Length, SocketFlags.None, ref epSender, new AsyncCallback(OnReceive), epSender);
            }
            catch (Exception ex)
            {
                Error(ex);   
            }
        }

        public void Error(Exception ex)
        {
            MessageBox.Show(ex.Message, "Server", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void OnReceive(IAsyncResult ar)
        {
            try
            {
                EndPoint epSender = (EndPoint)new IPEndPoint(IPAddress.Any, 0);

                serverSocket.EndReceiveFrom(ar, ref epSender);
                Data msgReceived = new Data(byteData);
                txtLog.Text += "Message From " + epSender.ToString();
                txtLog.AppendText(Environment.NewLine);
                byte[] message;

                if (msgReceived.CmdCommand == Command.BeginGame)
                {
                    ClientInfo newClient = new ClientInfo(epSender, msgReceived.UserMessage.Split(':')[0]);
                    message = new Data(Command.BeginGame, newClient.ToString()).ToByte();
                    Game currentGame = games[Convert.ToInt32(msgReceived.UserMessage.Split(':')[1]) / 30];
                    if (this.InvokeRequired)
                        BeginInvoke((MethodInvoker)delegate()
                        {
                            currentGame.sendMessage(newClient, message, epSender);
                        });
                    else
                        currentGame.sendMessage(newClient, message, epSender);
                }
                else if (msgReceived.CmdCommand == Command.LeaveGame)
                {
                    for(int i=0;i<8;i++)
                        if (this.InvokeRequired)
                            BeginInvoke((MethodInvoker)delegate()
                            {
                                games[i].leaveGame(epSender);
                            });
                        else
                            games[i].leaveGame(epSender);
                
                }

                serverSocket.BeginReceiveFrom(byteData, 0, byteData.Length, SocketFlags.None, ref epSender, new AsyncCallback(OnReceive), epSender);
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }

        public void OnSend(IAsyncResult ar)
        {
            try
            {
                serverSocket.EndSend(ar);
            }
            catch (Exception ex)
            {
                Error(ex);
            }
        }
    }

    class Game
    {
        private int countDown;
        private List<ClientInfo> players;
        private string currentString;
        private Timer countDownTimer;
        private static Socket serverSocket;

        public Game(Socket server)
        {
            serverSocket = server;
            countDownTimer = new Timer();
            countDownTimer.Interval = 1000;
            countDownTimer.Enabled = true;
            countDownTimer.Tick += new EventHandler(countDownTimer_Tick);
            initialize();
        }

        internal String getQuote()
        {
            String resposeValue = string.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://en.wikipedia.com/w/api.php?action=query&prop=extracts&format=xml&exsentences=3&exlimit=1&explaintext=&exsectionformat=plain&generator=random&grnnamespace=0&grnlimit=1");
            request.Method = "POST";
            request.ContentType = "application/xml";
            request.ContentLength = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            if (responseStream != null)
                                using (StreamReader reader = new StreamReader(responseStream))
                                {
                                    resposeValue = Regex.Replace(System.Net.WebUtility.HtmlDecode(Regex.Replace(reader.ReadToEnd().Split(new string[] { "<extract xml:space=\"preserve\">" }, StringSplitOptions.None)[1].Split(new string[] { "</extract>" }, StringSplitOptions.None)[0], @"[^\u0000-\u007F]", string.Empty).Replace("\n", string.Empty).Replace("*", "").Replace("|", "").Replace("/", "").Replace("\\", "")), @"\s\s+", " ").Trim();
                                }
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Check your internet connection", "Client", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return resposeValue.Substring(0, Math.Min(400, resposeValue.Length));
        }

        private void initialize()
        {
            countDownTimer.Stop();
            players = new List<ClientInfo>();
            currentString = getQuote();
            countDown = 15;
        }

        private void countDownTimer_Tick(object sender, EventArgs e)
        {
            countDown--;
            if (countDown == 5)
                initialize();
        }

        public void sendMessage(ClientInfo newClient, byte[] message, EndPoint epSender)
        {
            String playersString = "";
            foreach (ClientInfo c in players)
            {
                playersString += c.ToString();
                serverSocket.BeginSendTo(message, 0, message.Length, SocketFlags.None, c.endpoint, new AsyncCallback(OnSend), null);
            }
            playersString += countDown + "/" + currentString;
            message = new Data(Command.BeginGame, playersString).ToByte();
            serverSocket.BeginSendTo(message, 0, message.Length, SocketFlags.None, epSender, new AsyncCallback(OnSend), null);
            if (players.Count == 1)
            {
                countDown = 15;
                countDownTimer.Start();
            }
            players.Add(newClient);
        }

        public void OnSend(IAsyncResult ar)
        {
            try
            {
                serverSocket.EndSend(ar);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error");
            }
        }

        public void leaveGame(EndPoint epSender)
        {
            for (int i = 0; i < players.Count; i++)
                if (players[i].endpoint.ToString() == epSender.ToString())
                {
                    players.RemoveAt(i);
                    break;
                }
            if (players.Count == 1)
            {
                countDown = 15;
                countDownTimer.Stop();
                currentString = getQuote();
            }
        }
    }

    class Data
    {
        private String userMessage;
        private Command cmdCommand;

        public String UserMessage
        {
            get
            {
                return this.userMessage;
            }
            set
            {
                this.userMessage = value;
            }
        }

        public Command CmdCommand
        {
            get
            {
                return this.cmdCommand;
            }
            set
            {
                this.cmdCommand = value;
            }
        }

        public Data()
        {
            this.cmdCommand = Command.Null;
            this.userMessage = null;
        }

        public Data(Command cmdCommand, String userMessage)
        {
            this.CmdCommand = cmdCommand;
            this.UserMessage = userMessage;
        }

        public Data(byte[] data)
        {
            this.cmdCommand = (Command)BitConverter.ToInt32(data, 0);
            int msgLen = BitConverter.ToInt32(data, 4);
            UserMessage = msgLen > 0 ? Encoding.UTF8.GetString(data, 8, msgLen) : null;
        }

        public byte[] ToByte()
        {
            List<byte> result = new List<byte>();
            result.AddRange(BitConverter.GetBytes((int)cmdCommand));
            result.AddRange(BitConverter.GetBytes(UserMessage != null ? UserMessage.Length : 0));
            if (UserMessage != null) result.AddRange(Encoding.UTF8.GetBytes(UserMessage));
            return result.ToArray();
        }
    }
}